#!/usr/bin/env node
import { program } from "commander";
import inquirer from "inquirer";
import download from "download-git-repo";
import fs from "fs";
import path from "path";

program
    .arguments("<projectName> <port>")
    .description("Create a new React repository from Git")
    .action(async (projectName, port) => {
        const projectNameInput =
            projectName ||
            (
                await inquirer.prompt({
                    type: "input",
                    name: "projectName",
                    message: "Enter the project name:",
                })
            ).projectName;

        const portInput =
            port ||
            (
                await inquirer.prompt({
                    type: "input",
                    name: "port",
                    message: "Enter the port number for the application:",
                    validate: (input) => {
                        const isValid = /^\d+$/.test(input);
                        return isValid || "Please enter a valid port number.";
                    },
                })
            ).port;

        try {
            await downloadRepo(
                "direct:https://gitlab.com/dicky.julian/app-bnis-module.git#main",
                projectNameInput,
            );
            console.log("Repository downloaded successfully!");
            updateFederationConfigFile(projectNameInput, portInput);
            updatePackageConfigFile(projectNameInput, portInput);
            console.log("Configuration file updated successfully!");
        } catch (error) {
            console.error("Error:", error.message || error);
        }
    });

const downloadRepo = (repoUrl, projectName) => {
    return new Promise((resolve, reject) => {
        download(repoUrl, projectName, { clone: true }, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

function updateFederationConfigFile(name, port) {
    const currentDir = process.cwd();

    const configFile = path.resolve(currentDir + "/" + name, "federation.config.json");
    const config = JSON.parse(fs.readFileSync(configFile, "utf-8"));

    // Update the port in the configuration
    config.config.name = name.replaceAll('-', '_');
    config.port = port;

    fs.writeFileSync(configFile, JSON.stringify(config, null, 2));
}

function updatePackageConfigFile(name, port) {
    const currentDir = process.cwd();

    const configFile = path.resolve(currentDir + "/" + name, "package.json");
    const config = JSON.parse(fs.readFileSync(configFile, "utf-8"));

    // Update the port in the configuration
    config.name = name.replaceAll('-', '_');;

    fs.writeFileSync(configFile, JSON.stringify(config, null, 2));
}

program.parse(process.argv);