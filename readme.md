`create-app-bnis` is a command-line tool to quickly scaffold a new React repository with microfrontend capabilities using GitLab.

### Installation

You can install `create-app-bnis` globally using npm:

```bash
npm install -g create-app-bnis
```

### Usage

After installing, you can use the following command to create a new React repository:

```bash
npx create-app-bnis <projectName> <port>
```

### Options

- `<projectName>`: Specify the project name.
- `<port>`: Specify the port for the application.

### Features

Microfrontend Support: Easily set up a React repository with microfrontend architecture.
GitLab Integration: Utilizes GitLab for repository creation.

### Examples

Create a new React repository named "my-app" on port 3000:

```bash
npx create-app-bnis my-app 3000
```

### Configuration

The tool updates the federation.config.json file with the specified port.

### License

This project is licensed under the ISC License.

### Contribution

Feel free to contribute to the project by submitting issues or pull requests.